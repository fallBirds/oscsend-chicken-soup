<?php

abstract class Core {
    /* 是否调试 */

    const DEBUG = FALSE;

    /* 预设值 */

    protected $db, $prefix;

    function __construct() {
        header("Content-type: text/html; charset=utf-8");
        if (self::DEBUG == FALSE) {
            error_reporting(0);
        }
        if (substr(php_sapi_name(), 0, 3) != 'cli' && !self::DEBUG) {
            exit("Only run in cmd!");
        }

        $config = require dirname(__FILE__) . '/Config/config.php';

        $dsn = $config['DB_TYPE'] . ":host=" . $config['DB_HOST'] . ";dbname=" . $config['DB_NAME'];

        try {
            $this->db = new PDO($dsn, $config['DB_USER'], $config['DB_PWD']);
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            $this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $this->db->exec('SET NAMES UTF8');
        } catch (PDOException $e) {
            die("Error!: " . $e->getMessage() . "<br/>");
        }
        $this->prefix = $config['DB_PREFIX'];
    }

    abstract public function index();
}
