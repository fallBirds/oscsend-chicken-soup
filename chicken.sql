-- phpMyAdmin SQL Dump
-- version 4.1.8
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2014-10-21 09:35:46
-- 服务器版本： 5.6.15
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `chicken`
--

-- --------------------------------------------------------

--
-- 表的结构 `ck_learn`
--

CREATE TABLE IF NOT EXISTS `ck_learn` (
  `learn_id` int(11) NOT NULL AUTO_INCREMENT,
  `learn_title` varchar(255) NOT NULL,
  `learn_reply` varchar(255) NOT NULL,
  `learn_status` tinyint(1) NOT NULL,
  `learn_time` int(11) NOT NULL,
  PRIMARY KEY (`learn_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- 表的结构 `ck_post`
--

CREATE TABLE IF NOT EXISTS `ck_post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_page` int(11) NOT NULL COMMENT '采集页数',
  `post_content` text NOT NULL COMMENT '鸡汤内容',
  `post_send` tinyint(1) NOT NULL COMMENT '是否发送',
  `post_time` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`post_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6971 ;

-- --------------------------------------------------------

--
-- 表的结构 `ck_reply_record`
--

CREATE TABLE IF NOT EXISTS `ck_reply_record` (
  `reply_record_id` int(11) NOT NULL AUTO_INCREMENT,
  `record_id` bigint(20) NOT NULL,
  `user_name` varchar(128) NOT NULL,
  `content` varchar(255) NOT NULL,
  PRIMARY KEY (`reply_record_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='系统回复记录' AUTO_INCREMENT=22 ;

-- --------------------------------------------------------

--
-- 表的结构 `ck_reply_type`
--

CREATE TABLE IF NOT EXISTS `ck_reply_type` (
  `reply_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(128) NOT NULL,
  PRIMARY KEY (`reply_type_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
