<?php

require dirname(__FILE__) . '/Core.php';

class collect extends Core {

    private $page = '700';

    public function index() {
        $sth = $this->db->prepare("SELECT * FROM ck_post GROUP BY post_page ORDER BY post_id desc");
        $sth->execute();
        $result = $sth->fetch();
        if (!empty($result['post_page'])) {
            $this->page = $result['post_page'] - 1;
        }
        
        /**
         * 自己找采集的网站
         */
        $url = "http://xxx.com/index_{$this->page}.html";
        $curl = curl_init(); // 启动一个CURL会话	
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环	
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容	
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $tmpInfo = curl_exec($curl); // 执行操作
        curl_close($curl); // 关闭CURL会话	
        preg_match_all('/<div.*class="pic_text".*>.*<p>/', $tmpInfo, $matches);
        foreach ($matches[0] as $key => $value) {
            $array[] = strip_tags($value);
        }

        foreach ($array as $key => $value) {
            $sql = "INSERT INTO {$this->prefix}post(`post_page`, `post_content`, `post_send`, `post_time`) VALUES (:post_page, :post_content, :post_send, :post_time)";
            $sth = $this->db->prepare($sql);
            $sth->execute(array('post_page' => $this->page, 'post_content' => $value, 'post_send' => '0', 'post_time' => time()));
        }
    }

}

$mail = new collect ();
$mail->index();
