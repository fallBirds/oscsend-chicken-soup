<?php

require dirname(__FILE__) . '/Core.php';

/**
 * 发送鸡汤动弹
 */
class sendChicken extends Core {

    private $cookie = 'cookie.txt';

    public function index() {
        $this->login();
        $this->send();
    }

    /**
     * 登录帐号
     */
    private function login() {
        $url = "https://www.oschina.net/action/user/hash_login";
        /**
         * 自己想办法获取帐号和密码吧
         */
        $data = "email=此处填写帐号&pwd=此处填写密码&verifyCode=&save_login=1";

        $curl = curl_init(); // 启动一个CURL会话	
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址				
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查	
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2); // 从证书中检查SSL加密算法是否存在	
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器	
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转	
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求	
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包	
        curl_setopt($curl, CURLOPT_COOKIEJAR, $this->cookie); // 存放Cookie信息的文件名称	
        curl_setopt($curl, CURLOPT_COOKIEFILE, $this->cookie); // 读取上面所储存的Cookie信息	
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环	
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容	
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        curl_exec($curl); // 执行操作
        curl_close($curl); // 关闭CURL会话
    }

    /**
     * 发送鸡汤
     */
    private function send() {
        $post = $this->getChicken();
        $url = "http://my.oschina.net/action/tweet/pub";
        $data = "user=2251019&user_code=0jKslPXAzgUuMjqI9fcmMWDS09TpImsz3Y0DLH50&attachment=0&code_brush=&code_snippet=&msg={$post['post_content']}";
        $curl = curl_init(); // 启动一个CURL会话	
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址	
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求	
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
        curl_setopt($curl, CURLOPT_COOKIEFILE, $this->cookie); // 读取上面所储存的Cookie信息	
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环	
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容	
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $tmpInfo = curl_exec($curl); // 执行操作
        curl_close($curl); // 关闭CURL会话
        $this->recordSend($post['post_id']);
    }

    /**
     * 获取最新鸡汤
     */
    private function getChicken() {
        $sth = $this->db->prepare("SELECT * FROM {$this->prefix}post WHERE post_send = 0 ORDER BY post_id asc limit 1");
        $sth->execute();
        $result = $sth->fetch();
        return $result;
    }

    /**
     * 记录发送
     */
    private function recordSend($post_id) {
        $sth = $this->db->prepare("UPDATE {$this->prefix}post SET post_send = 1 WHERE post_id = :post_id");
        $sth->execute(array('post_id' => $post_id));
    }

}

$mail = new sendChicken();
$mail->index();
