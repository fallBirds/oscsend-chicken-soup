<?php

require dirname(__FILE__) . '/Core.php';

/**
 * 发送鸡汤动弹
 */
class reply extends Core {

    private $cookie = 'cookie.txt';

    public function index() {
        $this->login();
        $this->getContent();
    }

    /**
     * 登录帐号
     */
    private function login() {
        $url = "https://www.oschina.net/action/user/hash_login";
        /**
         * 填写你的帐号
         */
        $data = "email=&pwd=&verifyCode=&save_login=1";

        $curl = curl_init(); // 启动一个CURL会话	
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址				
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查	
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2); // 从证书中检查SSL加密算法是否存在	
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器	
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转	
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求	
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包	
        curl_setopt($curl, CURLOPT_COOKIEJAR, $this->cookie); // 存放Cookie信息的文件名称	
        curl_setopt($curl, CURLOPT_COOKIEFILE, $this->cookie); // 读取上面所储存的Cookie信息	
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环	
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容	
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        curl_exec($curl); // 执行操作
        curl_close($curl); // 关闭CURL会话
    }

    /**
     * 获取内容
     */
    public function getContent() {
        
        $replyType = $this->replyType();
        
        /**
         * 用户中心的地址。 ft=atme 就是@提醒的消息地址
         */
        $url = "http://my.oschina.net/u/2251019/?ft=atme";
        $curl = curl_init(); // 启动一个CURL会话	
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址	
        curl_setopt($curl, CURLOPT_COOKIEFILE, $this->cookie); // 读取上面所储存的Cookie信息	
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环	
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容	
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $tmpInfo = curl_exec($curl); // 执行操作
        curl_close($curl); // 关闭CURL会话
        
        /**
         * 下面开始就是匹配 @提醒的用户和条件
         */
        preg_match_all('/<td class="TweetContent">([\s\S]+?)<\/td>/', $tmpInfo, $allContent);

        foreach ($allContent[0] as $key => $value) {
        
            /**
             * 先获取@提醒的唯一ID值
             * 获取这个，用来跟数据库匹配是否已经存在了。
             */
            preg_match("/tweet_reply\((\d+)\)/", $value, $replyId);
            $recordId = preg_replace('/[^0-9]/', '', $replyId['0']);
            if ($this->exitReply($recordId)) {
                continue;
            }
            
            /**
             * 取名字
             */
            preg_match('/<a.*class="user">([\s\S]+?)<\/a>/', $value, $userName);
            $replyName = strip_tags($userName['0']);
            
            /**
             * 获取需要执行的指令
             */
            preg_match("/<div.*class='post'>([\s\S]+?)<\/div>/", $value, $cmd);

            foreach ($replyType as $k => $v) {
            
                /**
                 * 匹配一下指令
                 * 组装需要回复的内容
                 * 从这块代码开始，可以做得搞基点
                 * 如：加入学习指令呀。当然，学习指令这个需要另外写一个方法。
                 * 嗯，当你写到这里会发现上面代码重复很多，必须进行精简封装了。
                 * 目前我只写到这里了，没写更深入的。
                 */
                if (strpos($cmd[0], $v['type_name']) !== false) {
                    $replyConten = "";
                    $replyConten .= "@{$replyName} ";
                    $type = $this->getReplyType($v['type_name']);
                    if (empty($type)) {
                        $replyConten .= "非常抱歉，小鸡汤目前还没有找到符合的指令，你可以输入#学习#+内容让小鸡汤学习呀。";
                    } else {
                        $replyConten .= $type['learn_reply'];
                    }
                    $this->doReply($replyConten);

                    $this->recordReplySend($recordId, $replyName, $replyConten);
                }
            }
        }
    }

    /**
     * 回复类型
     */
    public function replyType() {
        $sth = $this->db->prepare("SELECT * FROM {$this->prefix}reply_type ");
        $sth->execute();
        return $sth->fetchAll();
    }

    /**
     * 最后的回复
     */
    private function exitReply($id) {
        $sth = $this->db->prepare("SELECT * FROM {$this->prefix}reply_record where record_id = :record_id ");
        $sth->execute(array('record_id' => $id));
        return $sth->fetch();
    }

    /**
     * 获取对应的回复内容
     * @param type $type
     */
    private function getReplyType($type) {
        $sth = $this->db->prepare("SELECT * FROM {$this->prefix}learn WHERE learn_title = :learn_title limit 1 ");
        $sth->execute(array('learn_title' => $type));
        return $sth->fetch();
    }

    /**
     * 发送鸡汤
     * @todo 你看!这里的代码明显是重复了。
     */
    private function doReply($content) {
        sleep(2);
        $url = "http://my.oschina.net/action/tweet/pub";
        $data = "user=&user_code=&attachment=0&code_brush=&code_snippet=&msg={$content}";
        $curl = curl_init(); // 启动一个CURL会话	
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址	
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求	
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
        curl_setopt($curl, CURLOPT_COOKIEFILE, $this->cookie); // 读取上面所储存的Cookie信息	
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环	
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容	
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $tmpInfo = curl_exec($curl); // 执行操作
        curl_close($curl); // 关闭CURL会话
    }

    /**
     * 记录已经发送
     */
    private function recordReplySend($id, $user, $content) {
        $sql = "INSERT INTO {$this->prefix}reply_record(`record_id`, `user_name`, `content`) VALUES (:record_id, :user_name, :content)";
        $sth = $this->db->prepare($sql);
        $sth->execute(array('record_id' => $id, 'user_name' => $user, 'content' => $content));
    }

}

$mail = new reply();
$mail->index();

